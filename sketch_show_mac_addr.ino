#include <ESP8266WiFi.h>

void setup() {
  Serial.begin(115200);

  byte mac_addr[6];
  WiFi.macAddress(mac_addr);
  char buf[20];
  Serial.println("");
  Serial.println("MAC Address :");
  sprintf(buf,"  %02X:%02X:%02X:%02X:%02X:%02X",
    mac_addr[0],
    mac_addr[1],
    mac_addr[2],
    mac_addr[3],
    mac_addr[4],
    mac_addr[5]
  );
  Serial.println(buf);
  sprintf(buf,"  %02x%02x%02x%02x%02x%02x",
    mac_addr[0],
    mac_addr[1],
    mac_addr[2],
    mac_addr[3],
    mac_addr[4],
    mac_addr[5]
  );
  Serial.println(buf);
}

void loop() {
  delay(1000);
}
